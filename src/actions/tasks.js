export const loadTasks = tasks => ({
    type: 'LOAD_TASKS',
    tasks
})

export const addTask = task => ({
    type: 'ADD_TASK',
    task
})

export const editTask = task => ({
    type: 'EDIT_TASK',
    task
})

export const removeTask = task => ({
    type: 'REMOVE_TASK',
    task
})