import React from 'react';
import PropTypes from 'prop-types'
import { StyleSheet, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { Button } from 'react-native-elements';
import {Style, ButtonStyle} from "@styles/Style";

import { AsyncStorage } from "react-native"

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

export default class Home extends React.Component {
  constructor (props) {
    super(props);

    this.click = this.click.bind(this);
  }

  click(){
    AsyncStorage.removeItem("TASKS")
    console.log("tasks supprimés")
  }

  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Home
        </Text>
        <Text style={styles.instructions}>
          This is great
        </Text>
        <Button large buttonStyle={ ButtonStyle.secondaryButton } title='Supprimer tasks' onPress={() => this.click()} />
      </View>
    )
  }
}