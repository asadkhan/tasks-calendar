import React from 'react';
import { View, ScrollView, StyleSheet, Image } from 'react-native';
import { DrawerItems, SafeAreaView } from 'react-navigation';

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });


export default class ContentDrawer extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <ScrollView>
      <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
        <View /*style={{justifyContent:'center', alignItems:'center', padding:20}}*/>
            <Image source={require('@assets/logo.jpg')} /*style={{ width:150, height:150, borderRadius:75 }}*/ style={{width:"100%", height:200}} />
        </View>
        <DrawerItems {...this.props} />
      </SafeAreaView>
    </ScrollView>
    )
  }
}