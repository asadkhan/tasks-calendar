import React from 'react';
import { Card, ListItem, Button, Icon } from 'react-native-elements';
import { StyleSheet, Text, View, Alert} from 'react-native';

export default class PlanningList extends React.Component {
  constructor (props) {
    super(props);
  }

  onRemove(task){
    Alert.alert(
      'Attention !',
      'Voulez-vous supprimer definitivement cette tâche ?',
      [
        {text: 'Supprimer', onPress: () => this.props.removeTask(task)},
        {text: 'Annuler'}
      ],
      { cancelable: false }
    )
  }

  render () {
    return (
        <View>
        {
          this.props.tasks.map((task, index) => {
            let description = task.type + " - le " + task.date + " à " + task.time;
            return (
              <Card title={description} key={task.id}>
                <Text style={{marginBottom: 10}}>
                  {task.title}
                </Text>
                <Icon raised name='edit' type='font-awesome' color='#000' onPress={() => this.props.navigation.push("PlanningEditForm", {task: task})} />
                <Icon raised name='remove' type='font-awesome' color='#000' onPress={() => this.onRemove(task) }/>
              </Card>
            );
          })
        } 
      </View>
    )
  }
}