import { connect } from 'react-redux'
import PlanningForm from '@components/planning/PlanningForm'
import { addTask, editTask } from '@actions/tasks'

const mapStateToProps = state => ({
  //tasks: getVisibleTodos(state.tasks)
})

const mapDispatchToProps = dispatch => ({
  addTask: task => dispatch(addTask(task)),
  editTask: task => dispatch(editTask(task)),
  removeTask: task => dispatch(removeTask(task))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanningForm)