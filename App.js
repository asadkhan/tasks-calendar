  import React from 'react';
  import { StyleSheet, Text, View } from 'react-native';
  import { Provider } from 'react-redux';
  import { createStore, applyMiddleware } from 'redux';
  
  import AppReducer from '@reducers';
  import { AppNavigator, middleware } from '@navigators/DrawerNavigator';
  
  import { AsyncStorage } from "react-native"
  import Autocomplete from 'react-native-autocomplete-input'


  import {loadTasks} from '@actions/tasks';
  import {searchPlaces} from '@services/maps/maps';
  
  const store = createStore(AppReducer, applyMiddleware(middleware));

  type Props = {};
  export default class App extends React.Component<Props> {
  
    state = {
      response: ''
    };
  
    componentDidMount() {
      searchPlaces("15 rue Guy de Maupassant, 60100 Creil");

      AsyncStorage.getItem('TASKS', (err, result) => {
        if(err) return;
        if(result) store.dispatch(loadTasks(result));
      })
    }

    /*const instructions = Platform.select({
      ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
      android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
    });*/
  
    /*callApi = async () => {
      const response = await fetch('http://192.168.1.49:3000');
      const body = await response.json();
  
      console.log(body.text);
  
      if (response.status !== 200) throw Error(body.message);
  
      return body;
    };*/
  
    render() {
      return (
        <Provider store={store}>
          <AppNavigator />
        </Provider>
      );
    }
  }  