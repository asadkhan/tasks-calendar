const HeaderData = {
    headerTitleStyle: {
        marginLeft: "auto",
        marginRight: "auto",
    },
    headerStyle: {
        backgroundColor: '#009587',
    },
    headerTintColor: '#fff',
}

export default HeaderData;